using MongoDB.Bson;
using relationsinmongodbaspnetcore.RepositoryServices;

namespace relationsinmongodbaspnetcore.Models
{
    [BsonCollection("people_files")]
    public class PersonFile : Document
    {
        public ObjectId PersonId { get; set; }

        public string Name { get; set; }

        public Person Person { get; set; }
    }
}