
using System;
using System.Collections.Generic;
using relationsinmongodbaspnetcore.RepositoryServices;

namespace relationsinmongodbaspnetcore.Models
{
    [BsonCollection("people")]
    public class Person : Document
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public IList<PersonFile> Files { get; set; }
    }
}