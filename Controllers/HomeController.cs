using System;
using System.Linq;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using relationsinmongodbaspnetcore.Models;
using relationsinmongodbaspnetcore.RepositoryServices;

namespace relationsinmongodbaspnetcore.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<Person> personRepository;
        private readonly IRepository<PersonFile> fileRepository;

        public HomeController(IRepository<Models.Person> personRepository, IRepository<Models.PersonFile> fileRepository)
        {
            this.personRepository = personRepository;
            this.fileRepository = fileRepository;
        }

        public IActionResult Index()
        {
            personRepository.InsertOne(new Person()
            {
                FirstName = "nima",
                LastName = "airyana",
                BirthDate = DateTime.UtcNow
            });

            var list = personRepository.FilterBy(person => true).ToList();

            return Content(System.Text.Json.JsonSerializer.Serialize(list));
        }

        public IActionResult Relation()
        {
            var person = personRepository.FindOne(person => true);

            fileRepository.InsertOne(new PersonFile()
            {
                PersonId = person.Id,
                Name = "title",
                Person = person
            });

            return Content(JsonSerializer.Serialize(fileRepository.FilterBy(file => true).ToList()));
        }
    }
}